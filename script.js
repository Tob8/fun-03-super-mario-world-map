function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.1806054015696, lng: 8.48946793781291},
        // 47.1806054015696, 8.48946793781291 === home
        // 34.668318925867936, 135.4302504750348 === nintendo world
        zoom: 15,
        mapId: '77b07883541099d2',
        mapTypeControl: false,
        fullscreenControl: false,
        streetViewControl: false,
      });

      // 1) Name  2) Lat  3) Lon  4) Image  5) Scale height  6) Scale widht
      const markers = [
          [
                "Railway Station",
                47.1803428788931,
                8.48608299002167,
                "assets/mario-green-tube-pipe.svg",
                35,
                35,
            ],
            [
                "Bus stop",
                47.18021949522814, 
                8.492372825249376,
                "assets/mario-green-tube-pipe.svg",
                35,
                35,
            ],
            [
                "Football",
                47.18215192790115,
                8.487351730118087,
                "assets/mario-super-stars.svg",
                35,
                35,
            ],
            [
                "Tennis",
                47.18296863302004,
                8.487909629571945,
                "assets/mario-super-stars.svg",
                35,
                35,
            ],
            [
                "Basketball",
                47.183158223489315, 
                8.489036157361873,
                "assets/mario-super-stars.svg",
                35,
                35,
            ],
            [
                "Swimming",
                47.177003479581565,
                8.493477895435802,
                "assets/mario-super-stars.svg",
                35,
                35,
            ],
            [
                "18\/7 Shopping",
                47.182560282049316,
                8.48418672347439,
                "assets/mario-question-block.svg",
                35,
                35,
            ],
            [
                "Tobi\'s Castle",
                47.18045226349903,
                8.490900237296016,
                "assets/mario-castle.svg",
                40,
                40,
            ],
        ];
        
        for (let i = 0; i < markers.length; i++) {
            const currMarker = markers[i];
            
            const marker = new google.maps.Marker({
              position: {lat: currMarker[1], lng: currMarker[2]},
              map,
              title: currMarker[0],
              icon: {
                  url: currMarker[3],
                  scaledSize: new google.maps.Size(currMarker[4], currMarker[5])
              },
              animation: google.maps.Animation.DROP
            });

            const infoWindow = new google.maps.InfoWindow({
                content: currMarker[0],
            });
      
            marker.addListener("click", () => {
                infoWindow.open(map, marker);
            });
      }


}

// 47.18045226349903, 8.490900237296016   === Home / Castle
// 47.1803428788931,  8.48608299002167    === Bahnhof
// 47.18021949522814, 8.492372825249376   === Bushaltestelle
// 47.18215192790115, 8.487351730118087   === Fussbalfeld
// 47.18296863302004, 8.487909629571945   === Tennisplatz
// 47.183158223489315, 8.489036157361873  === Basketballplatz
// 47.177003479581565, 8.493477895435802  === Badeplatz
// 47.182560282049316, 8.48418672347439   === 18/7 Shopping
